Comparator library that can be used to compare 2 API responses (HTTP/HTTPS)
It will run according to the smallest number of file lines.

How to run:
1. Import project to IntellIJ
2. Rebuild project
3. Run /src//main/java/appCompare/compare file

4. If you want to run a million compare request
insert request url to the testFile.txt and testFile2.txt on src/main/resource

