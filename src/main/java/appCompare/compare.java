package appCompare;


import java.io.*;
import java.io.FileNotFoundException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;


public class compare {
    public static void main(String[] args) throws IOException {
        try {
            String fileName = "testFile.txt";
            ClassLoader classLoader = compare.class.getClassLoader();
            InputStream inputStream = classLoader.getResourceAsStream(fileName);
            InputStreamReader streamReader = new InputStreamReader(inputStream);
            BufferedReader reader = new BufferedReader(streamReader);
            ArrayList<String> url_1 = new ArrayList<String>();
            for (String line; (line = reader.readLine()) != null;) {
                url_1.add(line);
            }

            String fileName2 = "testFile2.txt";
            ClassLoader classLoader2 = compare.class.getClassLoader();
            InputStream inputStream2 = classLoader2.getResourceAsStream(fileName2);
            InputStreamReader streamReader2 = new InputStreamReader(inputStream2);
            BufferedReader reader2 = new BufferedReader(streamReader2);
            ArrayList<String> url_2 = new ArrayList<String>();
            for (String line2; (line2 = reader2.readLine()) != null;) {
                url_2.add(line2);
            }
                int minimum = Math.min(url_1.size(),url_2.size());

            for (int i = 0; i < minimum; i++) {
                String res1;
                String res2;
                if (i >= url_1.size()) {
                    res1 = "";
                } else {
                    res1 = hitUrl(url_1.get(i));
                }

                if (i >= url_2.size()) {
                    res2 = "";
                } else {
                    res2 = hitUrl(url_2.get(i));
                }

                ObjectMapper mapper = new ObjectMapper();

                JsonNode tree1 = mapper.readTree(res1);
                JsonNode tree2 = mapper.readTree(res2);

                boolean areTheyEqual = tree1.equals(tree2);
                String result = "";
                if(areTheyEqual==true){
                    result = url_1.get(i)+" equals "+url_2.get(i);

                    System.out.println(result);

                }else {
                    result = url_1.get(i)+" not equals "+url_2.get(i);

                    System.out.println(result);

                }


        }


            } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }


    private static String hitUrl(String uri) throws IOException{
        // System.out.println(uri);
        URL url = new URL(uri);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");
        con.addRequestProperty("User-Agent", "");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        // System.out.println(content);
        in.close();
        con.disconnect();

        return content.toString();
    }
}
